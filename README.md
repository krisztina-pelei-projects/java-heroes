# RPG Heroes Application

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Java Console Application** with **Unit Testing**.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Background

This is a console application for a role playing game, where the player can select a character. Characters can equip items found in the game. 
The individual classes have restrictions on which kind of items they can equip.

There are four character classes in this game:

- Mage
- Ranger
- Rogue
- Warrior

They can equip armor of the following types:

- Cloth
- Leather
- Mail
- Plate

They can equip weapons of the following types:

- Axe
- Bow
- Dagger
- Hammer
- Staff
- Sword
- Wand

Characters can equip items in the following slots:

- Head
- Body
- Legs
- Weapon


## Install

- Install JDK 17
- Install Intellij
- Clone repository

## Usage

- Run the application
- Run tests by right-clicking on the `tests` folder and selecting `Run 'All Tests'`

## Maintainers

[Krisztina Pelei](https://gitlab.com/kokriszti)
