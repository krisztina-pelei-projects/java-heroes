import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import hero.Warrior;
import item.Armor;
import item.Weapon;
import org.junit.jupiter.api.Test;
import util.ArmorType;
import util.PrimaryAttribute;
import util.Slot;
import util.WeaponType;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void equipWeapon_levelTooHigh_ShouldThrowEx() {
        Weapon testWeapon = new Weapon("common Axe", 2, WeaponType.AXE, 7, 1.1);
        Warrior testWarrior = new Warrior("Ned Stark");

        String expectedErrorMessage = "You haven't reached the required level";

        Exception exception = assertThrows(InvalidWeaponException.class,
            () -> testWarrior.equipWeapon(testWeapon));
        String actualErrorMessage = exception.getMessage();
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    void equipArmor_levelTooHigh_ShouldThrowEx() {
        Armor testPlateArmor = new Armor("common plate armor", 2, ArmorType.PLATE, new PrimaryAttribute(1, 0, 0));
        Warrior testWarrior = new Warrior("Ned Stark");

        String expectedErrorMessage = "You haven't reached the required level";

        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equipArmor(testPlateArmor, Slot.BODY));
        String actualErrorMessage = exception.getMessage();
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    void equipWeapon_wrongType_ShouldThrowEx() {
        Weapon testBow = new Weapon("common Bow", 1, WeaponType.BOW, 12, 0.8);
        Warrior testWarrior = new Warrior("Ned Stark");

        String expectedErrorMessage = "You can't have this kind of weapon";

        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equipWeapon(testBow));
        String actualErrorMessage = exception.getMessage();
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    void equipArmor_wrongType_ShouldThrowEx() {
        Armor testClothArmor = new Armor("common cloth armor", 1, ArmorType.CLOTH, new PrimaryAttribute(0, 0, 5));
        Warrior testWarrior = new Warrior("Ned Stark");

        String expectedErrorMessage = "You can't have this kind of armor";

        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equipArmor(testClothArmor, Slot.BODY));
        String actualErrorMessage = exception.getMessage();
        assertEquals(expectedErrorMessage, actualErrorMessage);
    }

    @Test
    void equipWeapon_rightType_ShouldReturnTrue() throws InvalidWeaponException {
        Weapon testWeapon = new Weapon("common Axe", 1, WeaponType.AXE, 7, 1.1);
        Warrior testWarrior = new Warrior("Ned Stark");

        boolean expected = true;
        boolean actual =  testWarrior.equipWeapon(testWeapon);

        assertEquals(expected, actual);
    }

    @Test
    void equipArmor_rightType_ShouldReturnTrue() throws InvalidArmorException {
        Armor testPlateArmor = new Armor("common plate armor", 1, ArmorType.PLATE, new PrimaryAttribute(1, 0, 0));
        Warrior testWarrior = new Warrior("Ned Stark");

        boolean expected = true;
        boolean actual =  testWarrior.equipArmor(testPlateArmor, Slot.BODY);

        assertEquals(expected, actual);
    }

    @Test
    void calculateCharacterDPS_noWeapon_ShouldReturnDPS(){
        Warrior testWarrior = new Warrior("Ned Stark");

        double DPSNoWeapon = 1;
        int totalMainAttribute = Warrior.BASE_STRENGTH;
        double expectedDPS = DPSNoWeapon*(1 + (totalMainAttribute / 100.0));
        double actualDPS =  testWarrior.calculateCharacterDPS();

        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void calculateCharacterDPS_rightWeapon_ShouldReturnDPS() throws InvalidWeaponException{
        Weapon testWeapon = new Weapon("common Axe", 1, WeaponType.AXE, 7, 1.1);
        Warrior testWarrior = new Warrior("Ned Stark");

        testWarrior.equipWeapon(testWeapon);

        double DPSTestWeapon = 7 * 1.1;
        int totalMainAttribute = Warrior.BASE_STRENGTH;
        double expectedDPS = DPSTestWeapon*(1 + (totalMainAttribute / 100.0));
        double actualDPS =  testWarrior.calculateCharacterDPS();

        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void calculateCharacterDPS_rightWeaponRightArmor_ShouldReturnDPS() throws InvalidWeaponException, InvalidArmorException{
        Weapon testWeapon = new Weapon("common Axe", 1, WeaponType.AXE, 7, 1.1);
        Armor testPlateArmor = new Armor("common plate armor", 1, ArmorType.PLATE, new PrimaryAttribute(1, 0, 0));
        Warrior testWarrior = new Warrior("Ned Stark");

        testWarrior.equipWeapon(testWeapon);
        testWarrior.equipArmor(testPlateArmor, Slot.BODY);

        double DPSTestWeapon = 7 * 1.1;
        int totalMainAttribute = Warrior.BASE_STRENGTH + testPlateArmor.getAttributes().getStrength();
        double expectedDPS = DPSTestWeapon*(1 + (totalMainAttribute / 100.0));
        double actualDPS =  testWarrior.calculateCharacterDPS();

        assertEquals(expectedDPS, actualDPS);
    }

}