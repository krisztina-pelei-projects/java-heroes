import hero.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    @Test
    void heroConstructor_validInputs_shouldBeLevel1() {
        int expected = 1;
        Hero testHero = new Mage("Gandalf");
        int actual = testHero.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void levelUp_validInputs_shouldBeLevel2() {
        int expected = 2;
        Hero testHero = new Mage("Gandalf");
        testHero.levelUp();
        int actual = testHero.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    void mageConstructor_validInputs_attributesShouldBeCorrect() {
        int expectedStrength = Mage.BASE_STRENGTH;
        int expectedDexterity = Mage.BASE_DEXTERITY;
        int expectedIntelligence = Mage.BASE_INTELLIGENCE;

        Mage testMage = new Mage("Gandalf");
        int actualStrength = testMage.getBasePrimaryAttribute().getStrength();
        int actualDexterity = testMage.getBasePrimaryAttribute().getDexterity();
        int actualIntelligence = testMage.getBasePrimaryAttribute().getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void warriorConstructor_validInputs_attributesShouldBeCorrect() {
        int expectedStrength = Warrior.BASE_STRENGTH;
        int expectedDexterity = Warrior.BASE_DEXTERITY;
        int expectedIntelligence = Warrior.BASE_INTELLIGENCE;

        Warrior testWarrior = new Warrior("Ned Stark");
        int actualStrength = testWarrior.getBasePrimaryAttribute().getStrength();
        int actualDexterity = testWarrior.getBasePrimaryAttribute().getDexterity();
        int actualIntelligence = testWarrior.getBasePrimaryAttribute().getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void rangerConstructor_validInputs_attributesShouldBeCorrect() {
        int expectedStrength = Ranger.BASE_STRENGTH;
        int expectedDexterity = Ranger.BASE_DEXTERITY;
        int expectedIntelligence = Ranger.BASE_INTELLIGENCE;

        Ranger testRanger = new Ranger("Walker");
        int actualStrength = testRanger.getBasePrimaryAttribute().getStrength();
        int actualDexterity = testRanger.getBasePrimaryAttribute().getDexterity();
        int actualIntelligence = testRanger.getBasePrimaryAttribute().getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void rogueConstructor_validInputs_attributesShouldBeCorrect() {
        int expectedStrength = Rogue.BASE_STRENGTH;
        int expectedDexterity = Rogue.BASE_DEXTERITY;
        int expectedIntelligence = Rogue.BASE_INTELLIGENCE;

        Rogue testRogue = new Rogue("Robin Hood");
        int actualStrength = testRogue.getBasePrimaryAttribute().getStrength();
        int actualDexterity = testRogue.getBasePrimaryAttribute().getDexterity();
        int actualIntelligence = testRogue.getBasePrimaryAttribute().getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void mageLevelUp_validInputs_attributesShouldBeCorrect() {
        int expectedStrength = Mage.BASE_STRENGTH + Mage.LEVEL_UP_STRENGTH;
        int expectedDexterity = Mage.BASE_DEXTERITY + Mage.LEVEL_UP_DEXTERITY;
        int expectedIntelligence = Mage.BASE_INTELLIGENCE + Mage.LEVEL_UP_INTELLIGENCE;

        Mage testMage = new Mage("Gandalf");
        testMage.levelUp();
        int actualStrength = testMage.getBasePrimaryAttribute().getStrength();
        int actualDexterity = testMage.getBasePrimaryAttribute().getDexterity();
        int actualIntelligence = testMage.getBasePrimaryAttribute().getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void warriorLevelUp_validInputs_attributesShouldBeCorrect() {
        int expectedStrength = Warrior.BASE_STRENGTH + Warrior.LEVEL_UP_STRENGTH;
        int expectedDexterity = Warrior.BASE_DEXTERITY + Warrior.LEVEL_UP_DEXTERITY;
        int expectedIntelligence = Warrior.BASE_INTELLIGENCE + Warrior.LEVEL_UP_INTELLIGENCE;

        Warrior testWarrior = new Warrior("Ned Stark");
        testWarrior.levelUp();
        int actualStrength = testWarrior.getBasePrimaryAttribute().getStrength();
        int actualDexterity = testWarrior.getBasePrimaryAttribute().getDexterity();
        int actualIntelligence = testWarrior.getBasePrimaryAttribute().getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void rangerLevelUp_validInputs_attributesShouldBeCorrect() {
        int expectedStrength = Ranger.BASE_STRENGTH + Ranger.LEVEL_UP_STRENGTH;
        int expectedDexterity = Ranger.BASE_DEXTERITY + Ranger.LEVEL_UP_DEXTERITY;
        int expectedIntelligence = Ranger.BASE_INTELLIGENCE + Ranger.LEVEL_UP_INTELLIGENCE;

        Ranger testRanger = new Ranger("Walker");
        testRanger.levelUp();
        int actualStrength = testRanger.getBasePrimaryAttribute().getStrength();
        int actualDexterity = testRanger.getBasePrimaryAttribute().getDexterity();
        int actualIntelligence = testRanger.getBasePrimaryAttribute().getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    void rogueLevelUp_validInputs_attributesShouldBeCorrect() {
        int expectedStrength = Rogue.BASE_STRENGTH + Rogue.LEVEL_UP_STRENGTH;
        int expectedDexterity = Rogue.BASE_DEXTERITY + Rogue.LEVEL_UP_DEXTERITY;
        int expectedIntelligence = Rogue.BASE_INTELLIGENCE + Rogue.LEVEL_UP_INTELLIGENCE;

        Rogue testRogue = new Rogue("Robin Hood");
        testRogue.levelUp();
        int actualStrength = testRogue.getBasePrimaryAttribute().getStrength();
        int actualDexterity = testRogue.getBasePrimaryAttribute().getDexterity();
        int actualIntelligence = testRogue.getBasePrimaryAttribute().getIntelligence();

        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

}