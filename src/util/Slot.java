package util;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
