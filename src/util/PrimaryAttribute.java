package util;

public class PrimaryAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public PrimaryAttribute (PrimaryAttribute attribute) {
        this.strength = attribute.getStrength();
        this.dexterity = attribute.getDexterity();
        this.intelligence = attribute.getIntelligence();
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void raiseAttributes(int strength, int dexterity, int intelligence) {
        this.strength = this.strength + strength;
        this.dexterity = this.dexterity + dexterity;
        this.intelligence = this.intelligence + intelligence;
    }

    public void raiseAttributes(PrimaryAttribute attributeToAdd) {
        this.strength = this.strength + attributeToAdd.getStrength();
        this.dexterity = this.dexterity + attributeToAdd.getDexterity();
        this.intelligence = this.intelligence + attributeToAdd.getIntelligence();
    }

    @Override
    public String toString() {
        return "Strength: " + this.strength + " Dexterity: " + this.dexterity + " Intelligence: " + this.intelligence;
    }



}
