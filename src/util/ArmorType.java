package util;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
