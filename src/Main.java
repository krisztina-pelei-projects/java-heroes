import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import hero.Mage;
import item.Armor;
import item.Item;
import item.Weapon;
import util.ArmorType;
import util.PrimaryAttribute;
import util.Slot;
import util.WeaponType;

import java.util.Map;

public class Main {
    public static void main(String[] args) {

    //code for example game with printouts:
        Mage gandalf = new Mage("Gandalf");
        System.out.println(gandalf.getBasePrimaryAttribute().getIntelligence());
        gandalf.levelUp();
        System.out.println(gandalf.getLevel());
        System.out.println(gandalf.getBasePrimaryAttribute().getIntelligence());

        Weapon pickAxe = new Weapon("Pickaxe", 2, WeaponType.AXE, 5, 0.8);
        Weapon magicWand = new Weapon("Magic Wand", 2, WeaponType.WAND, 4, 0.8);


        //Gandalf equips wrong item(AXE)
        try{
            gandalf.equipWeapon(pickAxe);
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }

        //Gandalf equips right item(WAND)
        try{
            gandalf.equipWeapon(magicWand);
            System.out.println("Gandalf equipped Magic Wand");
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }

        //print Gandalf's equipment hashmap
        for (Map.Entry<Slot, Item> entry : gandalf.getEquipment().entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue().toString());
        }

        Weapon staff = new Weapon("Staff", 2, WeaponType.STAFF, 2, 0.6);

        //Gandalf replaces weapon (STAFF)
        try{
            gandalf.equipWeapon(staff);
            System.out.println("Gandalf equipped Staff");
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }

        //print Gandalf's equipment hashmap
        for (Map.Entry<Slot, Item> entry : gandalf.getEquipment().entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue().toString());
        }

        Armor plateArmor = new Armor("Plate armor", 2, ArmorType.PLATE, new PrimaryAttribute(2,0,1));
        Armor clothArmor = new Armor("Cloth armor", 2, ArmorType.CLOTH, new PrimaryAttribute(0,0,4));

        //Gandalf equips wrong armor(PLATE)
        try{
            gandalf.equipArmor(plateArmor, Slot.HEAD);
            System.out.println("Gandalf equipped plate armor");
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }

        //Gandalf equips right armor(CLOTH)
        try{
            gandalf.equipArmor(clothArmor, Slot.BODY);
            System.out.println("Gandalf equipped cloth armor");
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }

        //print Gandalf's equipment hashmap
        for (Map.Entry<Slot, Item> entry : gandalf.getEquipment().entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue().toString());
        }

        System.out.println("Base primary attributes: " + gandalf.getBasePrimaryAttribute());
        System.out.println("Total primary attributes: " + gandalf.getTotalPrimaryAttribute());

        //Gandalf equips additional armor(CLOTH)
        try{
            gandalf.equipArmor(clothArmor, Slot.HEAD);
            System.out.println("Gandalf equipped cloth armor");
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }

        //print Gandalf's equipment hashmap
        for (Map.Entry<Slot, Item> entry : gandalf.getEquipment().entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue().toString());
        }

        System.out.println("Base primary attributes: " + gandalf.getBasePrimaryAttribute());
        System.out.println("Total primary attributes: " + gandalf.getTotalPrimaryAttribute());
        System.out.println("Total primary attributes: " + gandalf.calculateTotalAttributes());

        System.out.println("Staff DPS: " + staff.calculateDPS());
        System.out.println("Gandalf DPS: " + gandalf.calculateCharacterDPS());

        System.out.println(gandalf.displayStats());

    }
}