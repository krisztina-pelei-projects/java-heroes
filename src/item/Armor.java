package item;

import util.ArmorType;
import util.PrimaryAttribute;

public class Armor extends Item{
    private ArmorType armorType;
    private PrimaryAttribute attributes;

    public Armor(String name, int requiredLevel, ArmorType armorType, PrimaryAttribute attributes) {
        super(name, requiredLevel);
        this.armorType = armorType;
        this.attributes = attributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }
    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public PrimaryAttribute getAttributes() {
        return attributes;
    }
    public void setAttributes(PrimaryAttribute attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
