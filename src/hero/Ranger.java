package hero;

import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import item.Armor;
import item.Weapon;
import util.ArmorType;
import util.PrimaryAttribute;
import util.Slot;
import util.WeaponType;

public class Ranger extends Hero {
    public static final int BASE_STRENGTH = 1;
    public static final int BASE_DEXTERITY = 7;
    public static final int BASE_INTELLIGENCE = 1;
    public static final int LEVEL_UP_STRENGTH = 1;
    public static final int LEVEL_UP_DEXTERITY = 5;
    public static final int LEVEL_UP_INTELLIGENCE = 1;


    public Ranger(String name) {
        super(name, new PrimaryAttribute(BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE));
    }

    @Override
    public void levelUp() {
        super.levelUp();
        basePrimaryAttribute.raiseAttributes(new PrimaryAttribute(LEVEL_UP_STRENGTH, LEVEL_UP_DEXTERITY, LEVEL_UP_INTELLIGENCE));
        this.totalPrimaryAttribute = calculateTotalAttributes();
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {

        if(level < weapon.getRequiredLevel()){
            throw new InvalidWeaponException("You haven't reached the required level");
        } else if(weapon.getWeaponType() == WeaponType.BOW) {
            getEquipment().put(Slot.WEAPON, weapon);
            return true;
        } else {
            throw new InvalidWeaponException("You can't have this kind of weapon");
        }
    }

    public boolean equipArmor(Armor armor, Slot slot) throws InvalidArmorException {
        if (level < armor.getRequiredLevel()) {
            throw new InvalidArmorException("You haven't reached the required level");
        } else if(armor.getArmorType() == ArmorType.LEATHER || armor.getArmorType() == ArmorType.MAIL) {
            getEquipment().put(slot, armor);
            this.totalPrimaryAttribute = calculateTotalAttributes();
            return true;
        } else {
            throw new InvalidArmorException("You can't have this kind of armor");
        }
    }

    @Override
    public double calculateCharacterDPS() {
        int totalMainAttribute = calculateTotalAttributes().getDexterity();
        return (getWeaponDPS() * (1 + totalMainAttribute/100.0));
    }
}
