package hero;

import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import item.Armor;
import item.Item;
import item.Weapon;
import util.PrimaryAttribute;
import util.Slot;

import java.util.HashMap;
import java.util.Map;

public abstract class Hero {
    protected String name;
    protected int level = 1;
    protected HashMap<Slot, Item> equipment = new HashMap<>();
    //stores attributes from character and level:
    protected PrimaryAttribute basePrimaryAttribute;
    //stores attributes from equipment:
    protected PrimaryAttribute totalPrimaryAttribute;


    public Hero(String name, PrimaryAttribute basePrimaryAttribute) {
        this.name = name;
        this.basePrimaryAttribute = basePrimaryAttribute;
        this.totalPrimaryAttribute = basePrimaryAttribute;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }
    public void setEquipment(HashMap<Slot, Item> equipment) {
        this.equipment = equipment;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }
    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttribute getBasePrimaryAttribute() {
        return basePrimaryAttribute;
    }
    public void setBasePrimaryAttribute(PrimaryAttribute basePrimaryAttribute) {
        this.basePrimaryAttribute = basePrimaryAttribute;
    }

    public PrimaryAttribute getTotalPrimaryAttribute() {
        return totalPrimaryAttribute;
    }
    public void setTotalPrimaryAttribute(PrimaryAttribute totalPrimaryAttribute) {
        this.totalPrimaryAttribute = totalPrimaryAttribute;
    }

    public abstract boolean equipWeapon(Weapon weapon) throws InvalidWeaponException;
    public abstract boolean equipArmor(Armor armor, Slot slot) throws InvalidArmorException;

    public abstract double calculateCharacterDPS();

    //calculates equipped weapon's DPS, to be used in calculating character's DPS:
    protected double getWeaponDPS() {
        double weaponDPS = 1;
        if (equipment.containsKey(Slot.WEAPON)) {
            Weapon actualWeapon = (Weapon) equipment.get(Slot.WEAPON);
            weaponDPS = actualWeapon.calculateDPS();
        }
        return weaponDPS;
    }

    public void levelUp() {
        this.level +=1;
    }

    public String displayStats () {
        StringBuilder sr = new StringBuilder();
        sr.append("Name: " + this.name + " - ");
        sr.append("Level: " + this.level  + " - ");
        sr.append("Strength: " + calculateTotalAttributes().getStrength()  + " - ");
        sr.append("Dexterity: " + calculateTotalAttributes().getDexterity()  + " - ");
        sr.append("Intelligence: " + calculateTotalAttributes().getIntelligence()  + " - ");
        sr.append("DPS: " + calculateCharacterDPS());

        return sr.toString();
    }

    //calculates total attributes from character, level and equipment bonus:
    public PrimaryAttribute calculateTotalAttributes () {
        //start with base attributes:
        PrimaryAttribute totalAttribute = new PrimaryAttribute(this.basePrimaryAttribute);

        //iterate through equipment and add bonus values:
        for (Map.Entry<Slot, Item> entry : this.equipment.entrySet()) {
            if (!(entry.getKey() == Slot.WEAPON)) {
                Armor armor = (Armor) entry.getValue();
                totalAttribute.raiseAttributes(armor.getAttributes());
            }
        }

        return totalAttribute;
    }

}
